#!/bin/bash 

function program_is_installed {
  local return_=1;
  type $1 >/dev/null 2>&1 || { local return_=0; }
  echo "$return_";
}

function install_program {
    if apt-get -qq install $1; then
        echo "Successfully installed $1";
    else
        echo "Error installing $1";
    fi
}


pkg=("virtualenv");
dir=("service/files/crp" "service/files/curves_reg" "service/files/curves_clf");

for i in "${pkg[@]}"
do
    if [ $(program_is_installed $i) == 1 ]; then
        echo "$i installed";
    else
        install_program $i;
    fi
done

for i in "${dir[@]}"
do
    if [ -d $i ]; then
        echo "$i existed";
    else
        mkdir -p $i;
        echo "$i create";
    fi
done

virtualenv --python=python3 env
source env/bin/activate
pip install -r requirements.txt
deactivate

# chmod -R 777 service
# chown -R www-data:www-data service
