"""Contain all config setting for module
machine learning
"""


import os
import sqlite3
import json
from datetime import datetime

import MySQLdb


BASEDIR = os.path.abspath(os.path.dirname(__file__))


MODEL_CURVES_REG = os.path.join(BASEDIR, 'files', 'curves_reg')


MODEL_CURVES_CLF = os.path.join(BASEDIR, 'files', 'curves_clf')


MODEL_ANFIS = os.path.join(BASEDIR, 'files', 'anfis')


MODEL_CRP = os.path.join(BASEDIR, 'files', 'crp')


SQLITE_PATH = os.path.join(BASEDIR, 'files', 'wipm-dev.db')


ROUTES = {
    'TRAIN_REGRESSION': '/wipm/regression/model',
    'PREDICT_REGRESSION': '/wipm/regression/predict',

    'TRAIN_CLASSIFICATION': '/wipm/classification/model',
    'PREDICT_CLASSIFICATION': '/wipm/classification/predict',

    'TRAIN_CRP': '/wipm/crp/model',
    'PREDICT_CRP': '/wipm/crp/predict',
    'ANFIS': '/wipm/anfis/predict',
    'DELETE_MODEL': '/wipm/models',

    'GET_RESULT_TRAIN': '/wipm/result/train',
    'GET_RESULT_PREDICT': '/wipm/result/predict',
    'GET_ALL_MODEL': '/wipm/models',
}


BROKER = {
    'TOPIC_JOBS': '/wi-ml/jobs',
    'USERNAME': 'wi-ml',
    'PASSWORD': '123456',
    'PROTOCOLS': 'websockets',
}


WIPM_CLIENT = {
    'ID': 'wipm',
    'HOST': '127.0.0.1',
    'PORT': 1883,
    'QOS': 2,
}


ML_CLIENT = {
    'ID': 'ml_service',
    'HOST': '127.0.0.1',
    'PORT': 1883,
    'QOS': 2,
}

db = MySQLdb.connect('localhost', 'wipm', '', 'wipm')
cur = db.cursor()

def get_now():
    return datetime.now().strftime(('%Y-%m-%d %H:%M:%S'))

def store_train_result(model_id, result):
    global cur
    global db 
    sql = "SELECT id FROM train WHERE model_id='%s'" % model_id 
    cur.execute(sql)
    instance = cur.fetchone()
    if instance is None:
        sql = "INSERT INTO train(model_id, result) VALUES ('%s','%s')" % (model_id, json.dumps(result))
        cur.execute(sql)
    else:               
        sql = "UPDATE train SET result='%s' WHERE model_id='%s'" % (json.dumps(result), model_id)
        cur.execute(sql)
    db.commit()

def del_train_result(model_id):
    global cur
    global db 
    sql = "DELETE FROM train WHERE model_id='%s'" % model_id 
    cur.execute(sql)
    db.commit()

def store_predict_result(model_id, result):
    global cur
    global db 
    sql = "SELECT id FROM predict WHERE model_id='%s'" % model_id
    cur.execute(sql)
    instance = cur.fetchone()
    if instance is None:
        sql = "INSERT INTO train(model_id, result) VALUES ('%s','%s')" % (model_id, json.dumps(result))
        cur.execute(sql)
    else:               
        sql = "UPDATE train SET result='%s' WHERE model_id='%s'" % (json.dumps(result), model_id)
        cur.execute(sql)
    db.commit()
