"""Design model Master/Worker
Perform parallel computing for tasks
machine learning
"""

import json
import logging
from datetime import datetime
from threading import Thread
import threading
from queue import Queue
from time import sleep

import paho.mqtt.client as mqtt
from paho.mqtt import publish

from .config import BROKER, ML_CLIENT, WIPM_CLIENT
from .config import get_now
from .ml_function import _regression_train, _regression_predict, \
    _crp_train, _crp_predict, _classification_train, \
    _classification_predict, _anfis


logging.basicConfig(filename='./logs/ml.log', level=logging.INFO)

def capture_threads(update_time=0.5):
    while True:
        now = get_now()
        _threads_string = ', '.join([_thread.getName() for _thread in threading.enumerate()])
        logging.info('[%s] Thread is alive %s' % (now, _threads_string))
        sleep(update_time)

def _publisher_on_connect(client, userdata, flags, rc):
    now = get_now()
    logging.info(msg='[%s] Publisher connected to broker' % now)

def _publisher_on_publish(client, userdata, mid):
    now = get_now()
    logging.info(msg='[%s] Publisher publish to broker' % now)


class Worker(Thread):
    """Process computing machine learning for task
    Result is push result queue for master
    """

    def __init__(self, tasks, results, *args, **kwargs):
        super(Worker, self).__init__()
        self._name = 'Worker ' + self._name
        self._tasks = tasks
        self._results = results
        self.daemon = True

    def run(self):
        while True:
            payload = self._tasks.get()
            if payload:
                try:
                    result = dict()
                    topic_result = payload['topic_result']
                    task = payload['task']
                    payload = payload['payload']
                    now = get_now()
                    logging.info(msg='[%s] %s is running' % (now, self._name))

                    if task == 'TRAIN_REGRESSION':
                        result = _regression_train(payload)
                    elif task == 'PREDICT_REGRESSION':
                        result = _regression_predict(payload)
                    elif task == 'TRAIN_CLASSIFICATION':
                        result = _classification_train(payload)
                    elif task == 'PREDICT_CLASSIFICATION':
                        result = _classification_predict(payload)
                    elif task == 'TRAIN_CRP':
                        result = _crp_train(payload)
                    elif task == 'PREDICT_CRP':
                        result = _crp_predict(payload)
                    elif task == 'ANFIS':
                        result = _anfis(payload)
                except Exception as err:
                    logging.exception('[%s] %s throw exception with message "%s"' % \
                                        (now, self._name, str(err)))
                    result.update(message=str(err), status=400)
                else:
                    logging.info(msg='[%s] %s done without error' % (now, self._name))
                    result.update(message='success', status=200)
                finally:
                    self._results.put((topic_result, result))
                    self._tasks.task_done()


class Publisher(Thread):
    """Process publish result
    Boost performance while limit bandwidth and many result
    """

    def __init__(self, results, *args, **kwargs):
        super(Publisher, self).__init__()
        self._name = 'Publisher ' + self._name
        self.daemon = True
        self._results = results

    def run(self):
        while True:
            topic, result = self._results.get()
            payload = json.dumps(result)
            if topic and result:
                try:
                    publish.single(
                        topic=topic,
                        payload=payload,
                        qos=ML_CLIENT['QOS'],
                        hostname=ML_CLIENT['HOST'],
                        port=ML_CLIENT['PORT'],
                        auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
                        retain=True,
                        transport=BROKER['PROTOCOLS'])
                    now = get_now()
                except Exception as err:
                    logging.exception('[%s] %s throw exception with message "%s"' % \
                                      (now, self._name, str(err)))
                else:
                    self._results.task_done()
                    logging.info(msg='[%s] Publisher %s publish result with topic "%s" without error' %\
                                (now, self._name, topic))
                finally:
                    logging.info(msg='[%s] Publisher %s done' % (now, self._name))


class Master(mqtt.Client):
    """Manage worker and publisher
    """

    def __init__(self, no_worker=5, no_publisher=5, *args, **kwargs):
        super(Master, self).__init__(*args, **kwargs)
        self._tasks = Queue()
        self._results = Queue()
        self._workers = list()
        self._publishers = list()
        for _ in range(no_worker):
            worker = Worker(self._tasks, self._results, _)
            worker.start()
            self._workers.append(worker)
        for _ in range(no_publisher):
            publisher = Publisher(self._results, _)
            publisher.start()
            self._publishers.append(publisher)

        # thread capture worker and publisher
        capture_thread = Thread(target=capture_threads, name='capture_threads')
        capture_thread.setDaemon(True)
        # capture_thread.run()

    def on_connect(self, client, userdata, flags, rc):
        now = get_now()
        self.subscribe(BROKER['TOPIC_JOBS']+'/#', WIPM_CLIENT['QOS'])
        logging.info(msg='[%s] ML process connected to broker' % now)

    def on_message(self, client, userdata, msg):
        now = get_now()
        payload = msg.payload.decode('utf-8')
        payload = json.loads(payload)
        self._tasks.put(payload)
        #self.publish(msg.topic, None, 0, False)
        logging.info(msg='[%s] ML process received from broker with topic %s' % \
                    (now, msg._topic))

    def on_disconnect(self, client, userdata, rc):
        now = get_now()
        for worker in self._workers:
            worker.join()
        for publisher in self._publishers:
            publisher.join()
        logging.info(msg='[%s] ML process disconnected' % now)
