"""Define and setting model regression
"""

import os
import pickle
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression as lr
from sklearn.linear_model import HuberRegressor as hr
from sklearn.linear_model import Lasso as ls
from sklearn.tree import DecisionTreeRegressor as dtr
from sklearn.ensemble import RandomForestRegressor as rfr
from xgboost import XGBRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.svm import SVR
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures

from .neural_network import MLPRegressor

class ModelBase(object):
    """Base class for all model regression
    """

    def __init__(self, name, *args, **kwargs):
        self.name = name
        self.steps = [('scaler', MinMaxScaler()),
                      ('normal', StandardScaler())]
        self.__setup__(*args, **kwargs)

    def fit(self, x_train, y_train, *args, **kwargs):
        self.pipeline.fit(x_train, y_train)

    def predict(self, x, *args, **kwargs):
        y_pred = self.pipeline.predict(x)
        return y_pred.reshape(-1)

    def evaluate(self, x_test, y_test, *args, **kwargs):
        y_pred = self.pipeline.predict(x_test)
        loss = mean_absolute_error(y_test, y_pred)
        return loss

    def save(self, path, *args, **kwargs):
        abspath = os.path.join(path, self.name)
        with open(abspath, mode='wb') as f:
            pickle.dump(self.pipeline, f)

    def load(self, path, *args, **kwargs):
        abspath = os.path.join(path, self.name)
        try:
            with open(abspath, mode='rb') as f:
                self.pipeline = pickle.load(f)
        except FileNotFoundError as error:
            raise FileNotFoundError('The model must be trained before it is used')
        else:
            pass
        finally:
            pass
    def __setup__(self, *args, **kwargs):
        pass

    def get_error_path(self, *args, **kwargs):
        return [], []


class LinearRegressor(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('poly', PolynomialFeatures(degree=kwargs.pop('degree', 2))))
        self.steps.append(('estimator', lr(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def fit(self, x_train, y_train, *args, **kwargs):
        x_train, x_test, y_train, y_test = \
            train_test_split(x_train, y_train, test_size=0.1)
        self.pipeline.fit(x_train, y_train)
        abs_err = self.evaluate(x_test, y_test)
        return abs_err


class HuberRegressor(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('poly', PolynomialFeatures(degree=kwargs.pop('degree', 2))))
        self.steps.append(('estimator', hr(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def fit(self, x_train, y_train, *args, **kwargs):
        x_train, x_test, y_train, y_test = \
            train_test_split(x_train, y_train, test_size=0.1)
        self.pipeline.fit(x_train, y_train)
        abs_err = self.evaluate(x_test, y_test)
        return abs_err


class Lasso(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('poly', PolynomialFeatures(degree=kwargs.pop('degree', 2))))
        self.steps.append(('estimator', ls(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def fit(self, x_train, y_train, *args, **kwargs):
        x_train, x_test, y_train, y_test = \
            train_test_split(x_train, y_train, test_size=0.1)
        self.pipeline.fit(x_train, y_train)
        abs_err = self.evaluate(x_test, y_test)
        return abs_err


class DecisionTreeRegressor(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('estimator', dtr(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def fit(self, x_train, y_train, *args, **kwargs):
        x_train, x_test, y_train, y_test = \
            train_test_split(x_train, y_train, test_size=0.1)
        self.pipeline.fit(x_train, y_train)
        abs_err = self.evaluate(x_test, y_test)
        return abs_err


class RandomForestRegressor(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('estimator', rfr(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def fit(self, x_train, y_train, *args, **kwargs):
        x_train, x_test, y_train, y_test = \
            train_test_split(x_train, y_train, test_size=0.1)
        self.pipeline.fit(x_train, y_train)
        abs_err = self.evaluate(x_test, y_test)
        return abs_err


class SupportVectorMachine(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('estimator', SVR(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def fit(self, x_train, y_train, *args, **kwargs):
        x_train, x_test, y_train, y_test = \
            train_test_split(x_train, y_train, test_size=0.1)
        self.pipeline.fit(x_train, y_train)
        abs_err = self.evaluate(x_test, y_test)
        return abs_err


class XGBoost(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('estimator', XGBRegressor(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def fit(self, x_train, y_train, *args, **kwargs):
        x_train, x_test, y_train, y_test = \
            train_test_split(x_train, y_train, test_size=0.1)
        self.pipeline.fit(x_train, y_train)
        abs_err = self.evaluate(x_test, y_test)
        return abs_err


class NeuralNetwork(ModelBase):
    def __setup__(self, *args, **kwargs):
        self.steps.append(('estimator', MLPRegressor(**kwargs)))
        self.pipeline = Pipeline(self.steps)

    def get_error_path(self, *args, **kwargs):
        return self.pipeline.named_steps['estimator'].lpath['train'], \
               self.pipeline.named_steps['estimator'].lpath['val']
