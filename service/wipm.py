"""Receive task from client, then push mosquitto broker
"""

import os
import json
import random
import sqlite3
import MySQLdb
from shutil import rmtree
from datetime import datetime

from flask import request, Flask
from flask import jsonify
from paho.mqtt import publish

from .config import BROKER, ROUTES, WIPM_CLIENT, ML_CLIENT, SQLITE_PATH, MODEL_CURVES_CLF, MODEL_CURVES_REG
from .verify import veriry_train_reg, veriry_predict_reg, veriry_train_clf, veriry_predict_clf,\
                    veriry_train_crp, veriry_predict_crp, verify_model_type
from .config import get_now, store_train_result

wipm = Flask(__name__)

@wipm.after_request
def apply_caching(response):
    """Solver issue CORS
    """
    response.headers["Access-Control-Allow-Origin"] = '*'
    response.headers["Access-Control-Allow-Methods"] = 'POST'
    response.headers["Access-Control-Allow-Headers"] = 'Content-Type'
    return response

@wipm.route('/')
def index():
    return jsonify(message='WIPM API', status=200), 200

@wipm.route(ROUTES['TRAIN_REGRESSION'], methods=['POST'])
@verify_model_type
@veriry_train_reg
def train_regression():
    try:
        req = request.get_json()
        topic_result = req.pop('topic')
        payload = dict(task='TRAIN_REGRESSION', topic_result=topic_result, payload=req)
        publish.single(
            topic=BROKER['TOPIC_JOBS']+'/'+topic_result,
            payload=json.dumps(payload),
            qos=WIPM_CLIENT['QOS'],
            hostname=WIPM_CLIENT['HOST'],
            port=WIPM_CLIENT['PORT'],
            auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
            transport=BROKER['PROTOCOLS'],
            client_id='WIPM'
        )
    except Exception as err:
        return jsonify(message=str(err), status=400), 400
    else:
        return jsonify(message="success", status=200), 200
    finally:
        pass

@wipm.route(ROUTES['PREDICT_REGRESSION'], methods=['POST'])
@veriry_predict_reg
def predict_regression():
    try:
        req = request.get_json()
        topic_result = req.pop('topic')
        payload = dict(task='PREDICT_REGRESSION', topic_result=topic_result, payload=req)
        publish.single(
            topic=BROKER['TOPIC_JOBS']+'/'+topic_result,
            payload=json.dumps(payload),
            qos=ML_CLIENT['QOS'],
            hostname=WIPM_CLIENT['HOST'],
            port=WIPM_CLIENT['PORT'],
            auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
            transport=BROKER['PROTOCOLS'],
            client_id='WIPM'
        )
    except Exception as err:
        return jsonify(message=str(err), status=400), 400
    else:
        return jsonify(message="success", status=200), 200
    finally:
        pass

@wipm.route(ROUTES['TRAIN_CLASSIFICATION'], methods=['POST'])
@verify_model_type
@veriry_train_clf
def train_classification():
    try:
        req = request.get_json()
        topic_result = req.pop('topic')
        payload = dict(task='TRAIN_CLASSIFICATION', topic_result=topic_result, payload=req)
        publish.single(
            topic=BROKER['TOPIC_JOBS']+'/'+topic_result,
            payload=json.dumps(payload),
            qos=ML_CLIENT['QOS'],
            hostname=WIPM_CLIENT['HOST'],
            port=WIPM_CLIENT['PORT'],
            auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
            transport=BROKER['PROTOCOLS'],
            client_id='WIPM'
        )
    except Exception as err:
        return jsonify(message=str(err), status=400), 400
    else:
        return jsonify(message="success", status=200), 200
    finally:
        pass

@wipm.route(ROUTES['PREDICT_CLASSIFICATION'], methods=['POST'])
@veriry_predict_clf
def predict_classification():
    try:
        req = request.get_json()
        topic_result = req.pop('topic')
        payload = dict(task='PREDICT_CLASSIFICATION', topic_result=topic_result, payload=req)
        publish.single(
            topic=BROKER['TOPIC_JOBS']+'/'+topic_result,
            payload=json.dumps(payload),
            qos=WIPM_CLIENT['QOS'],
            hostname=WIPM_CLIENT['HOST'],
            port=WIPM_CLIENT['PORT'],
            auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
            transport=BROKER['PROTOCOLS'],
            client_id='WIPM',
            retain=True
        )
    except Exception as err:
        return jsonify(message=str(err), status=400), 400
    else:
        return jsonify(message="success", status=200), 200
    finally:
        pass

@wipm.route(ROUTES['TRAIN_CRP'], methods=['POST'])
@veriry_train_crp
def train_crp():
    try:
        req = request.get_json()
        topic_result = req.pop('topic')
        payload = dict(task='TRAIN_CRP', topic_result=topic_result, payload=req)
        publish.single(
            topic=BROKER['TOPIC_JOBS']+'/'+topic_result,
            payload=json.dumps(payload),
            qos=ML_CLIENT['QOS'],
            hostname=WIPM_CLIENT['HOST'],
            port=WIPM_CLIENT['PORT'],
            auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
            transport=BROKER['PROTOCOLS'],
            client_id='WIPM'
        )
    except Exception as err:
        return jsonify(message=str(err), status=400), 400
    else:
        return jsonify(message="success", status=200), 200
    finally:
        pass

@wipm.route(ROUTES['PREDICT_CRP'], methods=['POST'])
@veriry_predict_crp
def predict_crp():
    try:
        req = request.get_json()
        topic_result = req.pop('topic')
        payload = dict(task='PREDICT_CRP', topic_result=topic_result, payload=req)
        publish.single(
            topic=BROKER['TOPIC_JOBS']+'/'+topic_result,
            payload=json.dumps(payload),
            qos=ML_CLIENT['QOS'],
            hostname=WIPM_CLIENT['HOST'],
            port=WIPM_CLIENT['PORT'],
            auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
            transport=BROKER['PROTOCOLS'],
            client_id='WIPM'
        )
    except Exception as err:
        return jsonify(message=str(err), status=400), 400
    else:
        return jsonify(message="success", status=200), 200
    finally:
        pass

@wipm.route(ROUTES['ANFIS'], methods=['POST'])
def anfis():
    try:
        req = request.get_json()
        topic_result = req.pop('topic')
        payload = dict(task='ANFIS', topic_result=topic_result, payload=req)
        publish.single(
            topic=BROKER['TOPIC_JOBS']+'/'+topic_result,
            payload=json.dumps(payload),
            qos=ML_CLIENT['QOS'],
            hostname=WIPM_CLIENT['HOST'],
            port=WIPM_CLIENT['PORT'],
            auth=dict(username=BROKER['USERNAME'], password=BROKER['PASSWORD']),
            transport=BROKER['PROTOCOLS'],
            client_id='WIPM'
        )
    except Exception as err:
        return jsonify(message=str(err), status=400), 400
    else:
        return jsonify(message="success", status=200), 200
    finally:
        pass

db = MySQLdb.connect('localhost', 'wipm', '', 'wipm')
cur = db.cursor()

@wipm.route(ROUTES['GET_RESULT_TRAIN'], methods=['POST'])
def get_result_train():
    try:
        result = dict()
        global cur
        global db
        model_id = request.get_json().get('model_id', None)
        if model_id is None:
            raise ValueError('model_id is missing')
        sql = "SELECT result FROM train WHERE model_id='%s'" % model_id
        cur.execute(sql)
        result = cur.fetchone()
        if result is None:
            raise ValueError('Model %s is missing' % model_id)
        result = result[0]
        result = json.loads(result)
    except Exception as error:
        return jsonify(message=str(error), status=400), 400
    else:
        return jsonify(result=result, status=200, message='success'), 200
    finally:
        pass

@wipm.route(ROUTES['DELETE_MODEL'], methods=['POST'])
def delete_model():
    try:
        result = dict()
        model_id = request.get_json().get('model_id', None)

        if model_id is None:
            raise ValueError('model_id is missing')
        else:
            model_path_reg = os.path.join(MODEL_CURVES_REG, model_id)
            model_path_clf = os.path.join(MODEL_CURVES_CLF, model_id)
            if os.path.isfile(model_path_reg):
                os.remove(model_path_reg)
            elif os.path.isfile(model_path_clf):
                os.remove(model_path_clf)
            elif os.path.isdir(model_path_clf):
                rmtree(model_path_clf)
            else:
                raise ValueError('Model %s not exist' % model_id)

            global cur
            global db
            cur.execute('DELETE FROM train WHERE model_id=?', (model_id,))
            db.commit()
            db.close()
    except Exception as error:
        return jsonify(message=str(error), status=200), 200
    else:
        return jsonify(message='success', status=200), 200
    finally:
        pass

@wipm.route(ROUTES['GET_ALL_MODEL'], methods=['GET'])
def get_all_model():
    try:
        result = dict(models=[], total=0)
        global cur
        global db
        cur.execute('SELECT model_id FROM train')
        result_iter = cur.fetchall()
        for row in result_iter:
            result['models'].append(row[0])
        result['total'] = len(result['models'])
        db.close()
    except Exception as error:
        return jsonify(message=str(error), status=400), 400
    else:
        return jsonify(result=result, status=200, message='success'), 200
    finally:
        pass
