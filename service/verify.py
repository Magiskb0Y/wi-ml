from functools import wraps

import numpy as np
from flask import request, jsonify

from .ml_toolkit.ml_toolkit import REGRESSOR, CLASSIFIER

MODEL_SUPPORTED = dict()
MODEL_SUPPORTED.update(REGRESSOR)
MODEL_SUPPORTED.update(CLASSIFIER)

def verify_model_type(endpoint_func):
    @wraps(endpoint_func)
    def decorator(*args, **kwargs):
        try:
            model_type = request.get_json()['model_type']
            if model_type not in MODEL_SUPPORTED:
                raise ValueError('Model %s not support' % model_type)
        except Exception as error:
            return jsonify(message=str(error), status=400), 400
        else:
            return endpoint_func(*args, **kwargs)
        finally:
            pass
    return decorator

def veriry_train_reg(endpoint_func):
    @wraps(endpoint_func)
    def decorator(*args, **kwargs):
        try:
            req = request.get_json()
            data = req['train']['data']
            target = req['train']['target']

            if len(data[0]) != len(target):
                raise ValueError('Number of samples=%d does not match number of target=%d' \
                                % (len(data[0]), len(target)))

        except Exception as error:
            return jsonify(message=str(error), status=400), 400
        else:
            return endpoint_func(*args, **kwargs)
        finally:
            pass
    return decorator

def veriry_predict_reg(endpoint_func):
    @wraps(endpoint_func)
    def decorator(*args, **kwargs):
        try:
            req = request.get_json()
            data = req['data']

        except Exception as error:
            return jsonify(message=str(error), status=400), 400
        else:
            return endpoint_func(*args, **kwargs)
        finally:
            pass
        return endpoint_func(*args, **kwargs)
    return decorator

def veriry_train_clf(endpoint_func):
    @wraps(endpoint_func)
    def decorator(*args, **kwargs):
        try:
            req = request.get_json()
            data = req['train']['data']
            target = req['train']['target']

            if len(data[0]) != len(target):
                raise ValueError('Number of samples=%d does not match number of target=%d' \
                                % (len(data[0]), len(target)))

            # check discrete variable
            for _y in target:
                if not isinstance(_y, int):
                    if isinstance(_y, float):
                        if not _y.is_integer():
                            raise ValueError('Target contain countinous variable, value %s' % str(_y))
                    else:
                        raise ValueError('Data type of value %s not supported' % str(_y))

            if len(list(set(target))) <= 1:
                raise ValueError('Number of class > 1')

        except Exception as error:
            return jsonify(message=str(error), status=400), 400
        else:
            return endpoint_func(*args, **kwargs)
        finally:
            pass
    return decorator

def veriry_predict_clf(endpoint_func):
    @wraps(endpoint_func)
    def decorator(*args, **kwargs):
        try:
            req = request.get_json()
            data = req['data']

        except Exception as error:
            return jsonify(message=str(error), status=400), 400
        else:
            return endpoint_func(*args, **kwargs)
        finally:
            pass
    return decorator

def veriry_train_crp(endpoint_func):
    @wraps(endpoint_func)
    def decorator(*args, **kwargs):
        try:
            req = request.get_json()
            data = req['train']['data']
            target = req['train']['target']
            params = req['params']

            # check discrete variable
            for _y in target:
                if not isinstance(_y, int):
                    if isinstance(_y, float):
                        if not _y.is_integer():
                            raise ValueError('Target contain countinous variable, value %s' % str(_y))
                    else:
                        raise ValueError('Data type of value %s not supported' % str(_y))
            if len(data[0]) != len(target):
                raise ValueError('Number of samples=%d does not match number of target=%d' \
                                % (len(data[0]), len(target)))
            if params['curve_number'] > len(data)-1:
                raise ValueError('Curve number not in curves of dataset')
            if params['facies_class_number'] not in list(set(target)):
                raise ValueError('Facies class %d not in target' % params['facies_class_number'])

        except Exception as error:
            return jsonify(message=str(error), status=400), 400
        else:
            return endpoint_func(*args, **kwargs)
        finally:
            pass
    return decorator


def veriry_predict_crp(endpoint_func):
    @wraps(endpoint_func)
    def decorator(*args, **kwargs):
        try:
            req = request.get_json()
            data = req['input']['data']

        except Exception as error:
            return jsonify(message=str(error), status=400), 400
        else:
            return endpoint_func(*args, **kwargs)
        finally:
            pass
    return decorator