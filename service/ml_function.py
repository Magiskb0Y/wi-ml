"""Contain all function machine learning
"""

import os
import pickle
import sqlite3
import json
from time import sleep
from shutil import rmtree
from datetime import datetime

import numpy as np
# import tensorflow as tf
# from tensorflow.python.framework import ops
# import keras.backend.tensorflow_backend as KTF
from sklearn.preprocessing import LabelEncoder

from .ml_toolkit import ml_toolkit
from .ml_toolkit.clf_util import *
from .ml_toolkit.anfis import Anfis
from .ml_toolkit.crp import get_data_from_json
from .ml_toolkit.crp import train
from .ml_toolkit.crp import predict
from .config import MODEL_ANFIS, MODEL_CRP, MODEL_CURVES_CLF, MODEL_CURVES_REG, SQLITE_PATH
from .config import store_train_result


def _regression_train(payload):
    result = dict()
    data = np.array(payload['train']['data']).T
    target = np.array(payload['train']['target'])
    model = ml_toolkit.ModelFactory.get_regression(name=payload['model_id'], \
                                type=payload['model_type'], **payload.get('params', {}))
    if payload['model_type'] == 'MultiPerceptron':
        model.fit(data, target)
        loss, val_loss = model.get_error_path()
        model.save(MODEL_CURVES_REG)
        result = dict(loss=loss, val_loss=val_loss)
    else:
        error = model.fit(data, target)
        model.save(MODEL_CURVES_REG)
        result = dict(error=error)

    store_train_result(payload['model_id'], result)

    return result


def _regression_predict(payload):
    result = dict()
    data = np.array(payload['data']).T
    model = ml_toolkit.ModelFactory.get_regression(payload['model_id'])
    model.load(MODEL_CURVES_REG)
    y_predict = model.predict(data)
    result = dict(target=y_predict.tolist())
    return result


def _classification_train(payload):
    result = dict()
    data = np.array(payload['train']['data']).T
    target = np.array(payload['train']['target']).reshape(-1, 1)

    train_set = np.concatenate((data, target), axis=1)

    model = ml_toolkit.ModelFactory.get_classifier(type=payload['model_type'], \
                                                   train_set=train_set)
    params = ml_toolkit.DEFAULT_PARAMS[payload['model_type']].copy()
    params.update(payload.get('params', {}))

    model_id = payload['model_id']

    filename = os.path.join(MODEL_CURVES_CLF, model_id)
    if os.path.exists(filename):
        if os.path.isdir(filename):
            rmtree(filename)
        else:
            os.remove(filename)

    if payload['model_type'] == 'NeuralNetClassifier':
        if params['batch_size'] == 0:
            params['batch_size'] = None
        model.structure(params.pop('hidden_layer_sizes'), params.pop('activation'))
    elif payload['model_type'] == 'S_SOM':
        if params['first_epoch_size'] == 0:
            params['first_epoch_size'] = None
        if params['second_epoch_size'] == 0:
            params['second_epoch_size'] = None

    # with tf.Session() as sess:
    #     KTF.set_session(sess)

    model.fit(**params)

    model.save(filename)

    result = model.his.copy()
    result['cm'] = model.get_cm_data_url(model_id)

    store_train_result(model_id, result)

    return result

def _classification_predict(payload):
    result = dict()
    data = np.array(payload['data']).T
    filename = os.path.join(MODEL_CURVES_CLF, payload['model_id'])
    try:
        model = load(filename)
    except FileNotFoundError:
        raise RuntimeError('The model must be trained before it is used')
    else:
        result = model.get_result(data)

    return result

def _crp_train(payload):
    result = dict()
    training_well_ts, dim, tau, epsilon, lambd, \
        percent, curve_number, facies_class_number, \
        model_id = get_data_from_json(payload)
    if facies_class_number not in training_well_ts[:, -1].reshape(-1):
        result = dict(message='Facies class not exist', status=400)

    train(training_well_ts, dim, tau, epsilon,
          lambd, percent, curve_number, facies_class_number, model_id)
    result = dict(message='success')

    store_train_result(payload['model_id'], result)

    return result


def _crp_predict(payload):
    result = dict()
    testset = payload['input']
    label_enc = LabelEncoder()
    well = label_enc.fit_transform(
        np.array(testset['well']).reshape(-1, 1)).reshape(-1, 1)
    testset['data'] = np.array(testset['data']).T
    testing_well_ts = np.concatenate((well, testset['data']), axis=1)
    predict_vector = predict(testing_well_ts, payload['model_id'])
    result = dict(target=predict_vector)

    return result


def _anfis(payload):
    result = dict()
    return result
