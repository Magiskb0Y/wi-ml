#!env/bin/python

""" Start WIPM service
"""

import logging
from service.wipm import wipm
from service.config import get_now


logging.basicConfig(filename='./logs/wipm.log', level=logging.INFO)


if __name__ == '__main__':
    try:
        now = get_now()
        wipm.run(host='0.0.0.0', port=9000, debug=True)
        logging.info(msg='[%s] WIPM is running...' % now)
    except RuntimeError as err:
        logging.exception(msg='[%s] %s' % (now, str(err)))
    else:
        logging.info(msg='[%s] WIPM stopped without error' % now)
    finally:
        logging.info(msg='[%s] Server turn off' % now)
