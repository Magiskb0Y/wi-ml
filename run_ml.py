#!env/bin/python

""" Start machine learning process with multithread
"""

import logging
from service.ml_service import Master
from service.ml_toolkit.anfis import Anfis
from service.config import ML_CLIENT, BROKER
from service.config import get_now
import keras
from datetime import datetime

if __name__ == '__main__':
    try:
        master = Master(client_id=ML_CLIENT['ID'],
                        transport=BROKER['PROTOCOLS'], clean_session=False,
                        no_worker=4,
                        no_publisher=4)
        master.username_pw_set(BROKER['USERNAME'], BROKER['PASSWORD'])
        master.connect(ML_CLIENT['HOST'], ML_CLIENT['PORT'],
                       bind_address='0.0.0.0')
        master.loop_forever()
        now = get_now()
        logging.info(msg='[%s] ML is running...' % now)
    except RuntimeError as err:
        logging.exception(msg='[%s] %s' % (now, str(err)))
    else:
        logging.info(msg="[%s] ML stopped without error" % now)
    finally:
        now = datetime.now().strftime(('%Y-%m-%d %H:%M:%S'))
        logging.info(msg='[%s] ML shutdown' % now)
