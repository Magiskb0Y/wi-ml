### WIPM service

Step 1: Setup mosquitto server

Install independence package

```sh
sudo apt get update
sudo apt-get install build-essential python quilt python-setuptools python3
sudo apt-get install libssl-dev uuid-dev daemon
sudo apt-get install cmake libc-ares-dev libwebsockets-dev
```

Install server

Download source code from [https://mosquitto.org/download/](https://mosquitto.org/download/)

Extract it and into folder source code

Edit config.mk    

```sh
WITH_WEBSOCKETS:=yes
```

```sh
make
sudo make install
```

Copy file mosquitto.conf from wi-ml to /etc/mosquitto/conf.d/mosquitto.conf (make if not exist)

Create database

```sh
sqlite3 service/files/wipm-dev.db < db.sql
```

Step 2: Run server and others by pm2

1. start-mosquitto.sh

2. ml.sh

3. wipm.sh

Done